""" Módulo de format_num()"""

def format_num(num):
	"""Dada una cadena de texto representando un número devuelve una cadena de
	texto representando el mismo número pero utilizando el punto como separador
	de miles en la parte entera y usando la coma como separador decimal.
	
	>>> format_num("12345678")
	'12.345.678'
	
	>>> format_num("12345678.1234")
	'12.345.678,1234'
	
	>>> format_num("-12345,6789")
	'-12.345,6789'
	
	>>> format_num("-0,1234")
	'-0,1234'
	
	Args:
		num (string): Cadena de texto representando un número.
	Returns:
		(string): Cadena de texto representando el mismo número pero utilizando
		el punto como separador de miles en la parte entera y usando la coma 
		como separador decimal.
	
	"""
	if num.find(".") > 0:
		entero = num.split(".")[0]
		decimal = num.split(".")[1]
	elif num.find(",") > 0:
		entero = num.split(",")[0]
		decimal = num.split(",")[1]
	else:
		entero = num
		decimal = False
	for x in range(len(entero)-3,0,-3):
		entero = entero[:x]+"."+entero[x:]
	if decimal:
		return entero+","+decimal
	else:
		return entero