acumulador = float(0)

print("### CALCULATOR 2000 ###")

while True:
	print("R: {}".format(acumulador))
	#INTRODUCIR OPERACIONES
	try:
		operacion = input()
	#FINALIZACIÓN DEL PROGRAMA
	except KeyboardInterrupt:
		print("Have a nice day!!!")
		break
	except EOFError:
		print("Have a nice day!!!")
		break
	#COMPROBAR OPERACIONES
	if len(operacion) > 1:
		#SUMA
		if operacion[0]=="+":
			try:
				num = float(operacion[1:])
				acumulador += num
			except:
				None
		#RESTA
		if operacion[0]=="-":
			try:
				num = float(operacion[1:])
				acumulador -= num
			except:
				None
		#PRODUCTO
		if operacion[0]=="*":
			try:
				num = float(operacion[1:])
				acumulador *= num
			except:
				None
		#DIVISIÓN
		if operacion[0]=="/":
			try:
				num = float(operacion[1:])
				acumulador /= num
			except:
				None
		#POTENCIA
		if operacion[0]=="^":
			try:
				num = float(operacion[1:])
				acumulador **= num
			except:
				None
	#INICIALIZAR A 0 EL ACUMULADOR
	elif operacion in ["c","C"]:
		acumulador = float(0)